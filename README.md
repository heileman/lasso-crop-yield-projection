# LASSO-crop-yield-projection

This repository contains the R scripts to produce the results and plots presented in the article "Projecting impacts of extreme weather events on crop yields using LASSO regression" by Heilemann et al. (2024). 

The code was run in R version 4.2.1, from the R project "data_repo.Rproj". Please find the required packages to run the code in the script "scripts/00_Libraries_lists.R". 
The subfolder "data_raw" contains the yield data as downloaded from the Federal and State Statistical Offices (https://www.regionalstatistik.de). 
The subfolder "data_processed" containes the processed yield data (converted in yield anomalies (%) for districts with > 12 observations) combined with the historical hydrometeorological features presented in the article. 
The subfolder "data_spatial" contains the district boundaries. 

The climate simulation data necessary to run scripts 03_06 - 05_02 can be requested from the corresponding author.
