### File description: 

# This is a R script to show the LASSO shrinkage profiles for increasing lambda value.
# The script creates 8 panels with the individual plots for each crop.
#

rm(list = ls())

#### Source libraries and lists ####
source("scripts/00_Libraries_lists.R")

# Open a PDF device to save the plots
pdf("plots/lasso_coefficient_plots.pdf", width = 10, height = 10, pointsize = 16)  

# Set up a panel layout 
par(mfrow = c(3, 3))

LASSO_model_function <- function(crop) {
  
  # Load data
  YieldMeteo=read_csv(paste("data_processed/historical_YieldMeteo_1999_2022_", list_crop[crop], ".csv", sep = ""))
  
  ######################################################################
  print(paste("The predictors for", crop ,"are:"))
  predictors_gen <- YieldMeteo[, intersect(names(YieldMeteo), unlist(list_crop_var[crop]))]
  print(colnames(predictors_gen))
  
  ## Merge with yield data
  YieldMeteo_sel <- bind_cols(YieldMeteo[c(1,2,6,7,8,9)], predictors_gen)
  
  # Order columns by grouping extreme events
  order_columns <- c("year", "comId", "yield", "avgYield_comId", "yield_anomaly", "yield_percentChange",
                     grep("^Tmin", colnames(YieldMeteo_sel), value = TRUE),
                     grep("^AF", colnames(YieldMeteo_sel), value = TRUE),
                     grep("^CR", colnames(YieldMeteo_sel), value = TRUE),
                     grep("^PS", colnames(YieldMeteo_sel), value = TRUE),
                     grep("^Tmax", colnames(YieldMeteo_sel), value = TRUE),
                     grep("^Low", colnames(YieldMeteo_sel), value = TRUE),
                     grep("^High", colnames(YieldMeteo_sel), value = TRUE)
  )
  
  # Reorder the columns
  YieldMeteo_sel <- YieldMeteo_sel[, c(order_columns)]
  
  ## rename yield_anomaly to yield_change
  names(YieldMeteo_sel)[6]="percent_change"
  
  ## Exclude NA's
  YieldMeteo_sel=na.omit(YieldMeteo_sel)
  
  #define response variable
  y <- YieldMeteo_sel$percent_change
  
  #define matrix of predictor variables (standardized coefficients)
  x_std <- data.matrix(scale(YieldMeteo_sel[-c(1:6)]))
  
  # Split data into training and testing sets
  set.seed(456)  # set seed for reproducibility
  n <- nrow(x_std)
  train_indices <- sample(1:n, 0.8 * n)  # 80% training, 20% testing
  x_train <- x_std[train_indices, ]
  y_train <- y[train_indices]
  x_test <- x_std[-train_indices, ]
  y_test <- y[-train_indices]
  
  ### Sample size (n) of training and test data set 
  n_train <- as.numeric(nrow(x_train))
  print(n_train)
  n_test <- as.numeric(nrow(x_test))
  print(n_test)
  
  ###############################################################################
  ###### LASSO model ############################################################
  ###############################################################################
  ########################################
  # Make predictions with LASSO 

  #perform k-fold cross-validation to find optimal lambda value
  set.seed(456) # set seed for reproducibility of CV
  cv_model <- cv.glmnet(x_train, y_train, alpha = 1, type.measure = "mse") # alpha = 1 for LASSO (L1 regularization)
  
  #find optimal lambda value that minimizes lambda plus SE
  best_lambda <- cv_model$lambda.1se # Using lambda min plus standard errors 
  best_lambda
  
  Lasso_model <- glmnet(x_train, y_train, alpha = 1, lambda = best_lambda, standardize = FALSE, intercept = FALSE) # without intercept 
  coef(Lasso_model)
  
  #Create LASSO with entire dataset for all possible lambda values
  Lasso_model_all <- glmnet(x_train, y_train, alpha = 1, intercept = FALSE)

  # Increase the size of axis titles
  par(cex.lab = 1.5)
  
  # Plot LASSO coeff. profiles
  plot(Lasso_model_all, xvar = "lambda", label = FALSE, ylab = "Standardized coefficients", cex.lab = 1) 
  # Add a title above the plot using mtext
  mtext(names_crop[crop], side = 3, line = 2.5)
  #lbs_fun(test)
  abline(abline(v = log(best_lambda)), col = "black", lty=2)

}
#############################################################
#### Apply function to different crops #####################
###########################################################

lapply(seq_along(list_crop), LASSO_model_function)

dev.off()

### Further processing of plot in CORELdraw
