# Load all necessary libraries in R 

# Data wringing 
library(readr)
library(readxl)
library(openxlsx)
library(dplyr)
library(magrittr)
library(tibble)
library(tidyverse)
library(sf)

# LASSO model 
library(glmnet)

# Data visualization
library(ggplot2)
library(tmap)
library(ggpubr)
library(patchwork)


#### Variables used here ####
list_crop <- c("oats" = "oats", 
               "potatoes" = "potatoes",
               "silage maize" = "siloMaize",
               "summer barley" = "summerBarley",
               "sugar beet" = "sugarBeet",
               "winter rape" = "winterRape",
               "winter barley" = "winterBarley",
               "winter wheat" = "winterWheat")

names_crop <- c("oats" = "Oats", 
                "potatoes" = "Potatoes",
                "silage maize" = "Silage maize",
                "summer barley" = "Spring barley",
                "sugar beet" = "Sugar beets",
                "winter rape" = "Rapeseed",
                "winter barley" = "Winter barley",
                "winter wheat" = "Winter wheat")


###########################################
######### Define predictors ##############
#########################################

## relevant variables and thresholds according to Gömann et al. (2015) and Peichl et al. (2021)
list_crop_var <- list(
  
  "oats" =     c("Tmin_20.04.", "Tmin_20.05.", # Late frost
                 "CR_oats.03.", "CR_oats.04.", "CR_oats.05.", "CR_oats.06.", "CR_oats.07.", "CR_oats.08.", 
                 "PS.03.", "PS.04.", # Precipitation scarcity in Mar and Apr 
                 "Tmax_30.05.", "Tmax_30.06.", "Tmax_30.07.", "Tmax_30.08.",  # Heat from May-Aug
                 "Low_SMI_03_04_05", "Low_SMI_06_07_08", # Drought in fall, spring and summer 
                 "High_SMI_03_04_05", "High_SMI_06_07_08" # Waterlogging in fall, spring and summer 
  ), 
  
  "potatoes" = c( "Tmin_20.04.",  "Tmin_20.05.", # Late frost
                  "CR_s.04.", "CR_s.05.", "CR_s.06.", "CR_s.07.", "CR_sugar.08.", "CR_s.09.", "CR_s.10.", 
                  "PS.04.", # Precipitation scarcity in Apr 
                  "Tmax_30.06.", "Tmax_30.07.", "Tmax_30.08.",  # Heat from Jul-Aug
                  "Low_SMI_09_10",  "Low_SMI_04_05", "Low_SMI_06_07_08", # Drought in fall, spring and summer 
                  "High_SMI_09_10", "High_SMI_04_05", "High_SMI_06_07_08" # Waterlogging in fall, spring and summer 
  ),
  
  "siloMaize"  =   c("Tmin_20.05.", # Late frost 
                     "CR_s.04.", "CR_s.05.", "CR_s.06.", "CR_s.07.", "CR_s.08.", "CR_s.09.", "CR_s.10.", # Continuous rain Apr-Oct
                     "PS.04.", # Precipitation scarcity in Apr 
                     "Tmax_28.07.", "Tmax_28.08.",  # Heat from Jul-Aug
                     "Low_SMI_09_10",  "Low_SMI_04_05", "Low_SMI_06_07_08", # Drought in fall, spring and summer 
                     "High_SMI_09_10", "High_SMI_04_05", "High_SMI_06_07_08" # Waterlogging in fall, spring and summer 
  ),
  "summerBarley" = c("Tmin_20.04.", "Tmin_20.05.", # Late frost
                     "CR_oats.03.", "CR_oats.04.", "CR_oats.05.", "CR_oats.06.", "CR_oats.07.", 
                     "PS.03.", "PS.04.", # Precipitation scarcity in Mar and Apr 
                     "Tmax_30.05.", "Tmax_30.06.", "Tmax_30.07.",  # Heat from May-Jul
                     "Low_SMI_03_04_05", "Low_SMI_06_07", # Drought in fall, spring and summer 
                     "High_SMI_03_04_05", "High_SMI_06_07" # Waterlogging in fall, spring and summer 
  ),
  "sugarBeet" = c("Tmin_25.04.", "Tmin_25.10.", # Late and early frost
                  "CR_sugar.03.", "CR_sugar.04.", "CR_sugar.05.", "CR_sugar.06.", "CR_sugar.07.", "CR_sugar.08.", "CR_sugar.09.", "CR_sugar.10.", "CR_sugar.11.", "CR_sugar.12.",
                  "PS.05.", # Precipitation scarcity in May
                  "Tmax_30.07.", "Tmax_30.08.",  # Heat from Jul-Aug
                  "Low_SMI_09_10_11",  "Low_SMI_03_04_05", "Low_SMI_06_07_08", # Drought in fall, spring and summer 
                  "High_SMI_09_10_11", "High_SMI_03_04_05", "High_SMI_06_07_08" # Waterlogging in fall, spring and summer 
  ),
  "winterRape" = c("Tmin_20.10._lag", "Tmin_20.11._lag", "Tmin_20.12._lag", "Tmin_20.01.", "Tmin_20.02.", "Tmin_20.03.", "Tmin_20.04.", "Tmin_20.05.", # Black and late frost 
                   "AF.02", "AF.03", "AF.04", # Alternating frost from Feb-Apr
                   "CR_rape.09._lag", "CR_rape.10._lag" , "CR_rape.11._lag", "CR_rape.12._lag", "CR_rape.01.", "CR_rape.02.", "CR_rape.03.", "CR_rape.04.", "CR_rape.05.", "CR_rape.06.", "CR_rape.07.",# Continuous rain 
                   "PS.02.", "PS.03.", "PS.04.", "PS.05.", # Precipitation scarcity from Feb-May 
                   "Tmax_30.04.", "Tmax_30.05.",  # Heat from Apr-May
                   "Low_SMI_09_10_11_LAG", "Low_SMI_12_LAG_01_02", "Low_SMI_03_04_05", "Low_SMI_06_07", # Drought in fall, winter, spring and summer 
                   "High_SMI_09_10_11_LAG", "High_SMI_12_LAG_01_02", "High_SMI_03_04_05", "High_SMI_06_07" # Waterlogging in fall, winter, spring and summer 
  ), 
  "winterBarley" = c("Tmin_20.10._lag", "Tmin_20.11._lag", "Tmin_20.12._lag", "Tmin_20.01.", "Tmin_20.02.", "Tmin_20.03.", "Tmin_20.04.", "Tmin_20.05.", # Black and late frost 
                     "AF.02", "AF.03", "AF.04", # Alternating frost from Feb-Apr
                     "CR_w.10._lag", "CR_w.11._lag", "CR_w.12._lag", "CR_w.01.", "CR_w.02.", "CR_w.03.", "CR_w.04.", "CR_w.05.", "CR_w.06.", "CR_w.07.", # Continuous rain 
                     "PS.02.", "PS.03.", "PS.04.", "PS.05.", # Precipitation scarcity from Feb-May 
                     "Tmax_30.05.", "Tmax_30.06.",  # Heat from May-June
                     "Low_SMI_09_10_11_LAG", "Low_SMI_12_LAG_01_02", "Low_SMI_03_04_05", "Low_SMI_06_07", # Drought in fall, winter, spring and summer 
                     "High_SMI_09_10_11_LAG", "High_SMI_12_LAG_01_02", "High_SMI_03_04_05", "High_SMI_06_07" # Waterlogging in fall, winter, spring and summer 
  ),
  "winterWheat"  = c("Tmin_20.10._lag", "Tmin_20.11._lag", "Tmin_20.12._lag", "Tmin_20.01.", "Tmin_20.02.", "Tmin_20.03.", "Tmin_20.04.", "Tmin_20.05.", # Black and late frost 
                     "AF.02", "AF.03", "AF.04", # Alternating frost from 01.02.-30.04.
                     "CR_w.10._lag", "CR_w.11._lag", "CR_w.12._lag", "CR_w.01.", "CR_w.02.", "CR_w.03.", "CR_w.04.", "CR_w.05.", "CR_w.06.", "CR_w.07.", "CR_w.08.", # Continuous rain 
                     "PS.03.", "PS.04.", "PS.05.", # Precipitation scarcity from Mar-May 
                     "Tmax_30.05.", "Tmax_30.06.",  # Heat from May-June
                     "Low_SMI_10_11_LAG", "Low_SMI_12_LAG_01_02", "Low_SMI_03_04_05", "Low_SMI_06_07_08", # Drought in fall, winter, spring and summer 
                     "High_SMI_10_11_LAG", "High_SMI_12_LAG_01_02", "High_SMI_03_04_05", "High_SMI_06_07_08" # Waterlogging in fall, winter, spring and summer 
  ))


## Lists for climate projection models 

list_met <- c("met001", "met002", #"met003",
                  # "met004", "met005", "met006", "met007", "met008", "met009", "met010",
                  "met011", "met012", "met014", #"met015", 
                  "met016", "met017", "met018",
                  "met019", #"met020", 
                  "met021", #"met022", 
                  "met023", "met024", "met025",# "met026",
                  "met027", "met028", #"met029", 
                  "met030", "met031", "met032", "met033", "met034","met035", "met036", "met037", "met038", "met039", "met040", "met041", "met042",
                  "met043", "met044", "met045", "met046",# "met047", 
                  "met048", "met049", "met050",
                  "met051", #"met052", 
                  "met054", "met056", "met057", "met058",
                  "met059", "met060", "met061", "met062", "met063", "met064", "met065", "met066",
                  "met067", "met068", #"met069", 
                  "met070", "met071", "met072", "met073", "met074",
                  "met075", "met076", "met077", "met078", "met079", "met080", #"met081", 
                  "met082",
                  #"met083", 
                  "met084", "met085", "met086", "met087", "met088")



### Climate model lists
climate_model_list <- list("rcp2p6" = c("met003", "met004",
                                        "met011", "met012", "met014", "met015", # Model 013 missing !!!
                                        "met032", "met038", "met039",
                                        "met042", "met043", "met044", "met045",
                                        "met059",
                                        "met060", "met061", "met062",
                                        "met079",
                                        "met080", "met088"), #20 models (21 models in ensemble)
                           "rcp4p5" = c("met005", "met006",
                                        "met016", "met017", "met018", "met019",
                                        "met020",
                                        "met033", "met034",
                                        "met046", "met047", "met048", "met049",
                                        "met063", "met064", "met065", "met066",
                                        "met081"), # 18 models
                           "rcp8p5" = c("met001", "met002", "met007", "met008", "met009",
                                        "met010",
                                        "met021", "met022", "met023", "met024", "met025", "met026", "met027", "met028", "met029",
                                        "met030", "met031", "met035", "met036", "met037",
                                        "met040", "met041",
                                        "met050", "met051", "met052", "met054", "met056", "met057", "met058", ### Model met053 and met055 missing !!!
                                        "met067", "met068", "met069",
                                        "met070", "met071", "met072", "met073", "met074", "met075", "met076", "met077", "met078",
                                        "met082", "met083", "met084", "met085", "met086", "met087")) # 47 models (49 models in ensemble)


